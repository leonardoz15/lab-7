public class TodoItem {

    //declarations
    private int id;
    private static int nextId = 0;
    private String priority;
    private String category;
    private String task;
    private boolean done;

    //Constructor that creates the TodoItem method

    public TodoItem(String p, String c, String t) {
        id = nextId;
        nextId++;
        priority = p;
        category = c;
        task = t;
        done = false;
    }

    //Creates the getId method to return the value in the id instance
    public int getId() {
        return id;
    }


    //Creates getPriority method that returns the string in the priority
    public String getPriority() {
        return priority;
    }

    //Creates getCategory method and returns the category string
    public String getCategory() {
        return category;
    }

    //Creates the getTask method and returns the string inside the task

    public String getTask() {
        return task;
    }

    //Creates the markCompleted method in which if this is true will mark the program as completed
    public void markCompleted() {
        done = true;
    }

    //Creates the isCompleted method which completes the program
    public boolean isCompleted() {
        return done;
    }

    //Creates the toString method that returns the values inside all of the above instances
    public String toString() {
        return new String(id + ", " + priority + ", " + category + ", " + task + ", completed? " + done);
    }

}
