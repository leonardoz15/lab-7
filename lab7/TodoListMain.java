import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;

public class TodoListMain {

    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to the Todo List Manager!");
        System.out.println("What operation would you like to perform?");
        System.out.println("Available options: read, priority-search, category-search, completed, list, quit");

        Scanner scanner = new Scanner(System.in);
        TodoList todoList = new TodoList();

        //Create a while loop that will always execute as long as there is something to read inside of the scanner
        while(scanner.hasNext()) {
            String command = scanner.next();
            //If the scanner receives "read" as an input it reads the items in the todoList
            if(command.equals("read")) {
                todoList.readTodoItemsFromFile();
            }
            //If the scanner holds "list" it prints out the items in the file of todoList
            else if(command.equals("list")) {
                System.out.println(todoList.toString());
            }
            //If the scanner holds "completed" then marks the the chosenId as completed
            else if(command.equals("completed")) {
                System.out.println("What is the id of the task?");
                int chosenId = scanner.nextInt();
                todoList.markTaskAsCompleted(chosenId);
            }

            //Creates an if statement in which if the scanner holds "priority-search" then it will prompt the user to input which priority the want and create an iterator to print out the requested Priority
            else if(command.equals("priority-search"))
            {
                System.out.println("Which priority are you searching for? ");
                String requestedPriority = scanner.next();
                Iterator it = todoList.findTasksOfPriority(requestedPriority);
                while(it.hasNext()) {
                    System.out.println(it.next());

                }
            }

            //Creates an if statement in which if the scanner holds "category-search" then it will prompt the user to input which priority the want and create an iterator to print out the requested Category

            else if(command.equals("category-search"))
            {
                System.out.println("Which category are you searching for? ");
                String requestedCategory = scanner.next();
                todoList.findTasksOfCategory(requestedCategory);
                Iterator it2 = todoList.findTasksOfCategory(requestedCategory);
                while (it2.hasNext()) {
                    System.out.println(it2.next());
                }

            }

            else if(command.equals("quit")) {
                break;
            }



        }
    }
}

