import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class TodoList {

    private ArrayList<TodoItem> todoItems;
    private static final String TODOFILE = "todo.txt";

    public TodoList() {
        todoItems = new ArrayList<TodoItem>();//Creates the todoItems list
    }

    public void addTodoItem(TodoItem todoItem) {
        todoItems.add(todoItem);//Adds anything in todoItem to the list todoItems
    }

    public Iterator getTodoItems() {
        return todoItems.iterator();//Makes todoItems into an iterator to return the elements in todoItems seperately
    }

    public void readTodoItemsFromFile() throws IOException {
        Scanner fileScanner = new Scanner(new File(TODOFILE)); //Creates a scanner called fileScanner where what ever is in the todolist file is read.
        while(fileScanner.hasNext()) {//Creates a while loop that  as long as there is something to read in the scanner, some task will compute.
            String todoItemLine = fileScanner.nextLine();//Initializes todoItemLine as the the user input
            Scanner todoScanner = new Scanner(todoItemLine);//Creates another Scanner
            todoScanner.useDelimiter(",");//Uses the comma as a delimiter
            String priority, category, task;//Categorizes each section of the text
            priority = todoScanner.next();
            category = todoScanner.next();
            task = todoScanner.next();
            TodoItem todoItem = new TodoItem(priority, category, task);
            todoItems.add(todoItem);
        }
    }

    public void markTaskAsCompleted(int toMarkId) {
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getId() == toMarkId) {
                todoItem.markCompleted();
            }
        }
    }

    public Iterator findTasksOfPriority(String requestedPriority) {
        ArrayList<TodoItem> priorityList = new ArrayList<TodoItem>();
        // TODO: Add source code that will find and return all tasks of the requestedPriority
        Iterator iterator = todoItems.iterator();
        //Created a while loop that will read all elements inside of the Iterator of the todoItems method that will return all the items inside of the requested Priority
        while(iterator.hasNext()) {
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getPriority().equals(requestedPriority)){
            priorityList.add(todoItem);

            }
        }
        return priorityList.iterator();
    }

    public Iterator findTasksOfCategory(String requestedCategory) {
        ArrayList<TodoItem> categoryList = new ArrayList<TodoItem>();
        // TODO: Add source code that will find and return all tasks for the requestedCategory
        Iterator iterator = todoItems.iterator();
        //Created a while loop that will read all elements inside of the Iterator of the todoItems method that will return all the items inside of the requested Category

        while(iterator.hasNext()) {
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getCategory().equals(requestedCategory)){
            categoryList.add(todoItem);
            }
        }
        return categoryList.iterator();
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            buffer.append(iterator.next().toString());
            if(iterator.hasNext()) {
                buffer.append("\n");
            }
        }
        return buffer.toString();
    }

}
